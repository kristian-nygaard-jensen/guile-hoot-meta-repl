all: repl.wasm

repl.wasm: repl.scm
	guile repl.scm

serve: repl.wasm
	guile web-server.scm

clean:
	rm repl.wasm
