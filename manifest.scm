(use-modules (guix)
             (guix build-system gnu)
             (guix gexp)
             (guix git)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (gnu packages autotools)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages pkg-config)
             (gnu packages texinfo))

(define guile-hoot
  (let ((commit "edf87534f9d440ec4b56add05a2881fd37b464ec")
        (revision "1"))
    (package
      (name "guile-hoot")
      (version (git-version "0.2.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/spritely/guile-hoot.git")
                      (commit commit)))
                (file-name (git-file-name "guile-hoot" version))
                (sha256
                 (base32 "1kns7g0727csnmhd5p3ra24zzrjsshywxwmwyfn1768zfxsc454w"))))
      (build-system gnu-build-system)
      (arguments
       '(#:make-flags '("GUILE_AUTO_COMPILE=0")
         #:tests? #f))
      (native-inputs
       (list autoconf automake pkg-config texinfo))
      (inputs
       (list guile-next))
      (synopsis "WASM compiler for Guile Scheme")
      (description "Guile-hoot is an ahead-of-time WebAssembly compiler for GNU Guile.")
      (home-page "https://spritely.institute/hoot/")
      (license (list license:asl2.0 license:lgpl3+)))))


(packages->manifest (list guile-next guile-hoot gnu-make))
